# VLMC-Maestro

VLMC-Maestro is a simple "coordinator" between WebVLMC and VLMC that automates
the job of starting a VLMC instance for each frontend instance. It is a free
and open source software distributed under the terms of the [GPLv2].


## How to run?

After installing Python3, PIP and virtualenv, run

```
virtualenv flask
flask/bin/pip install flask
```

Then, build the VLMC in server mode. Lastly, place the executable in the same
folder as app.py.

To start:

```
./app.py
```

[VLMC]: https://code.videolan.org/videolan/vlmc
[GPLv2]: <https://opensource.org/licenses/GPL-2.0>
[freenode]: <http://www.freenode.net/>
[Freenode Web]: <http://webchat.freenode.net/>
[how to send patches]: <https://wiki.videolan.org/Sending_Patches_VLC/>
[Development mailing-list]: https://mailman.videolan.org/listinfo/vlmc-devel

