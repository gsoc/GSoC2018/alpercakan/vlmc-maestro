#!flask/bin/python

###############################################################################
# Copyright (C) 2018 VideoLAN
#
# Authors: Alper Çakan <alpercakan98@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
###############################################################################

from random import randint
from threading import Thread
import secrets
from flask import Flask, jsonify, request, abort, make_response
import subprocess
import os
import time

DEBUG_MODE = True
SOCKET_ADDRESS = 'ws://127.0.0.1'

app = Flask(__name__)

@app.route('/api/instance', methods=['POST'])
def create_instance():
  token = secrets.token_hex() # Random secret key
  port = randint(6000, 7000) # Random port

  vlmc_params = ['./vlmc',
                 './project.vlmc',
                 str(port),
                 token,
                 request.environ.get('REMOTE_ADDR', ''),
                 request.environ.get('HTTP_ORIGIN', '')]
  print(request.environ.get('REMOTE_ADDR', ''))
  instance = subprocess.Popen(vlmc_params)

  content = jsonify(
    {
      'authToken': token,
      'socketAddress': f'{SOCKET_ADDRESS}:{port}'
    }
  )

  resp = make_response(content)
  resp.headers['Access-Control-Allow-Origin'] = '*'
  return resp

if __name__ == '__main__':
  app.run(debug=DEBUG_MODE)
